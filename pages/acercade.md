---
layout: page
title: Del zacate al papel
permalink: /about
---

**Del zacate al papel** Es una publicación diseñada para visibilizar las voces estudiantiles y potenciar su pensamiento crítico. El nombre surge de la propuesta hecha por las personas estudiantes del curso de "Estructuras de datos" de la carrera de Ingeniería en computación en el Tecnológico de Costa Rica, Centro Académico de Alajuela.


Donde, en discusiones hechas sobre el zacate frente al Aula 303, las personas estudiantes decidieron cuestionarse su realidad inmediata, su contexto cultural y su futura profesión. Invitamos a las personas lectoras a abandonar el adulto-centrísmo, a profundizar en cada uno de nuestros números,  a analizar y discutir la computación, más allá de un código, desde una perspectiva joven y llena de entusiasmo por formar un nuevo mundo, uno nuestro y para todas las personas.

Si desean contactarnos pueden escribir a nuestro [Votán](mailto:ausanabria@itcr.ac.cr)
